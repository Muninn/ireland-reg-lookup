This library is a client for accessing the [carregistrationapi.com SOAP server API endpoint](http://ie.carregistrationapi.com/?a=10468) for Irish car registration information located at:
http://www.regcheck.org.uk/api/CheckIreland.asmx

The WSDL is located at: http://www.regcheck.org.uk/api/reg.asmx?wsdl

Check their main website for other endpoints for other countries: [Car Registration Info](http://ie.carregistrationapi.com/?a=10468)

## Example Number Plates (for testing)

Both of the numbers below as of comitting return a valid SOAP response containing 2 vehicles.

* 141-D-22960
* 151WH1745

That last reg results in s vardump of:

/home/leonidas/Code/ireland-reg-lookup/src/Client.php:91:
class stdClass#3 (1) {
  public $CheckIrelandResult =>
  class stdClass#4 (2) {
    public $vehicleJson =>
    string(778) "{
  "ABICode": "32130947",
  "Description": "MERCEDES-BENZ C 220 SPORT BLUETEC",
  "RegistrationYear": "2015",
  "CarMake": {
    "CurrentTextValue": "MERCEDES-BENZ"
  },
  "CarModel": {
    "CurrentTextValue": "C"
  },
  "BodyStyle": {
    "CurrentTextValue": "Saloon"
  },
  "Transmission": {
    "CurrentTextValue": "Automatic"
  },
  "FuelType": {
    "CurrentTextValue": "Diesel"
  },
  "MakeDescription": {
    "CurrentTextValue": "MERCEDES-BENZ"
  },
  "ModelDescription": {
    "CurrentTextValue": "C"
  "...
    public $vehicleData =>
    class stdClass#5 (12) {
      public $ABICode =>
      class stdClass#6 (1) {
        ...
      }
      public $Description =>
      class stdClass#7 (0) {
        ...
      }
      public $RegistrationYear =>
      class stdClass#8 (1) {
        ...
      }
      public $CarModel =>
      class stdClass#9 (1) {
        ...
      }
      public $BodyStyle =>
      class stdClass#10 (1) {
        ...
      }
      public $EngineSize =>
      class stdClass#12 (1) {
        ...
      }
      public $NumberOfDoors =>
      class stdClass#14 (1) {
        ...
      }
      public $Transmission =>
      class stdClass#16 (1) {
        ...
      }
      public $FuelType =>
      class stdClass#18 (1) {
        ...
      }
      public $MakeDescription =>
      class stdClass#20 (1) {
        ...
      }
      public $ModelDescription =>
      class stdClass#21 (0) {
        ...
      }
      public $NumberOfSeats =>
      class stdClass#22 (1) {
        ...
      }
    }
  }
}
