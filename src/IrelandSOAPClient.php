<?php
namespace Muninn;

/**
 * Include your private configuration data here including your username
 * which is required for the SOAP API call to work.
 *
 * @example
 * ```php
 *
 * <?php
 * $config = [];
 * $config['username'] = 'yourusername';
 * $config['password'] = 'yourpassword';
 * ?>
 *
 * ```
 *
 * Note: Password is not required.
 */
include('config.php');
/**
 * This is the main API Client file to the Irish car registration SOAP endpoint.
 *
 * When used this client will return a PHP object containing the
 * variables of a car based on a provided Irish car registration
 * plate.
 *
 * @author Doug Bromley
 * @copyright Doug Bromley
 * @license BSD
 * @link https://gitlab.com/Muninn/ireland-reg-lookup
 *
 * For full information concerning the license see the LICENSE file. For
 * more information on the package as a whole check the composer.json.
 */

use \SoapVar;
use \SoapHeader;
use \SoapClient;
use \SoapFault;

class IrelandSOAPClient
{
	/**
	 * @var string The user name used to login to your
	 *      	   {@link http://www.carregistrationapi.com/}
	 *             dashboard. It is also displayed in your dashboard area.
	 */
	protected $username;

	/**
	 * @var object The core PHP SOAP client provided to the class to work with. Having
	 *      	   been given a valid WSDL file location. For this class to work
	 *             and return details of Irish cars it expects:
	 *             {@link http://www.regcheck.org.uk/api/reg.asmx?wsdl}
	 */
	public $client;


	public function __construct($client, $username)
	{
		$this->client = $client;
		$this->username = $username;
	}

	/**
	 * Get car details either as an object from a JSON string or as an
	 * associative array
	 *
	 * @param  string  $registration The car Irish car registration.
	 * @param  boolean $assocarray   whether to return an associative array or object
	 *
	 * @return array|object 		 dependent on second parameter will
	 *                               return either associative array or object
	 */
	public function getCarDetails($registration, $assocarray = true)
	{
		try {
			$result = $this->client->CheckIreland([
				'RegistrationNumber' => $registration,
				'username' => $this->username
			]);

			return json_decode($result->CheckIrelandResult->vehicleJson, $assocarray));
		} catch (\SoapFault $e) {
			echo "SOAP Exception thrown";
			echo
			var_dump($e);
		}
	}
}

// Setup
$options = array(
	'username' => $config['username'],
	'trace' => true
);

$client = new SoapClient(
	'http://www.regcheck.org.uk/api/reg.asmx?wsdl',
	$options
);

$ieClient = new IrelandSOAPClient($client, $config['username']);

$result = $ieClient->getCarDetails('151WH1745');

var_dump($result);


